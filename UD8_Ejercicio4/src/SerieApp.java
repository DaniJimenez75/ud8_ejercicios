import dto.Serie;

public class SerieApp {

	public static void main(String[] args) {
		Serie s1 = new Serie();
		System.out.println("CONSTRUCTOR POR DEFECTO:");
		System.out.println(s1);
		System.out.println();
		Serie s2 = new Serie("Breaking Bad", "Vince Gilligan");
		System.out.println("Un constructor con el titulo y creador. El resto por defecto:");
		System.out.println(s2);
		System.out.println();
		Serie s3 = new Serie("Breaking Bad", 5, "Drama", "Vince Gilligan");
		System.out.println("Un constructor con todos los atributos, excepto de entregado:");
		System.out.println(s3);


	}

}
