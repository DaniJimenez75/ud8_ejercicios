package dto;

public class Serie {

		private String titulo;
		private int nTemporadas;
		private boolean entregado;
		private String genero;
		private String creador;
		
		//CONSTRUCTOR POR DEFECTO
		public Serie() {
			super();
			this.titulo="";
			this.nTemporadas=3;
			this.entregado=false;
			this.genero="";
			this.creador="";

			
		}
		
		
		//Un constructor con el titulo y creador. El resto por defecto
		public Serie(String titulo, String creador) {
			super();
			this.titulo = titulo;
			this.nTemporadas=3;
			this.entregado=false;
			this.genero="";
			this.creador = creador;
		}


		//Un constructor con todos los atributos, excepto de entregado
		public Serie(String titulo, int nTemporadas, String genero, String creador) {
			super();
			this.titulo = titulo;
			this.nTemporadas = nTemporadas;
			this.genero = genero;
			this.creador = creador;
		}



		
		@Override
		public String toString() {
			return "Serie [titulo=" + titulo + ", nTemporadas=" + nTemporadas + ", entregado=" + entregado + ", genero="
					+ genero + ", creador=" + creador + "]";
		}
		
		
		
		
		
		
		
}
