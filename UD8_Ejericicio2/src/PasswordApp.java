import dto.Password;

public class PasswordApp {

	public static void main(String[] args) {
		Password p1 = new Password();
		System.out.println("CONSTRUCTOR POR DEFECTO:");
		System.out.println(p1);
		Password p2 = new Password(10);
		System.out.println();
		System.out.println("Un constructor con la longitud que nosotros le pasemos. Generara una contraseņa aleatoria con esa longitud:");
		System.out.println(p2);

	}

}
