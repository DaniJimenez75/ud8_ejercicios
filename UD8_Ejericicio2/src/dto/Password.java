package dto;

import java.util.Random;

public class Password {
	
	private int longitud;
	private String contrase�a;
	
	//CONSTRUCTOR POR DEFECTO
	public Password() {
		this.longitud=8;
		this.contrase�a="";
	}

	
	//n constructor con la longitud que nosotros le pasemos. Generara una contrase�a aleatoria con esa longitud
	public Password(int longitud) {
		Random r = new Random();		
		this.longitud = longitud;
		
		int contador = 0;
		String password = "";
		while(contador<longitud) {//Mientras el contador sea mas peque�o que la longitud seguir� dentro del bucle
			
			if(contador % 2 == 0) {//Cuando el contador sea par generar letras
				char c = (char)(r.nextInt(26) + 'a');
				password = password + c;
			}else {//Si no generara numeros
				int n = r.nextInt(9);
				password = password + n;
			}

			contador++;
		}
		
		this.contrase�a = password;//Cuando termine guardaremos la contrase�a
	}

	@Override
	public String toString() {
		return "Password [longitud=" + longitud + ", contrase�a=" + contrase�a + "]";
	}
	
	
	
	
	
	

}
