package dto;

public class Persona {
		
	
	
		private String nombre;
		private int edad;
		private String dni;		
		private final char letra = 'H';
		private char sexo;
		private double peso;
		private double altura;
		
		//CONSTRUCTOR POR DEFECTO
		public Persona() {
			this.nombre="";
			this.edad=0;
			this.dni="12345678A";
			this.sexo= this.letra;
			this.peso=0;
			this.altura=0;
			
			
		}
		//Un constructor el nombre, edad y sexo, el resto por defecto
		public Persona(String nombre, int edad, char sexo) {
			this.nombre=nombre;
			this.edad=edad;
			this.dni="12345678A";
			this.sexo= sexo;
			this.peso=0;
			this.altura=0;
		}
		
		//Un constructor todos los atributos como parámetro
		public Persona(String nombre, int edad, String dni, char sexo, double peso, double altura) {
			this.nombre = nombre;
			this.edad = edad;
			this.dni = dni;
			this.sexo = sexo;
			this.peso = peso;
			this.altura = altura;
		}

		@Override
		public String toString() {
			return "Persona [nombre=" + nombre + ", edad=" + edad + ", dni=" + dni + ", letra=" + letra + ", sexo="
					+ sexo + ", peso=" + peso + ", altura=" + altura + "]";
		}
		
		
		
		
		
		


		
		
		
		
}
