import dto.Persona;

public class PersonaApp {

	public static void main(String[] args) {
		Persona p1 = new Persona();
		System.out.println("CONSTRUCTOR POR DEFECTO:");
		System.out.println(p1);
		System.out.println();
		Persona p2 = new Persona("Dani", 20, 'H');
		System.out.println("Un constructor el nombre, edad y sexo, el resto por defecto:");
		System.out.println(p2);
		System.out.println();
		Persona p3 = new Persona("Claudia", 21, "87654321B", 'M', 65.5, 1.80);
		System.out.println("Un constructor todos los atributos como parámetro:");
		System.out.println(p3);
		

	}

}
