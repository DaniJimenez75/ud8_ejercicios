import dto.Electrodomestico;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class ElectrodomesticoApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Electrodomestico e1 = new Electrodomestico();
		System.out.println("CONSTRUCTOR POR DEFECTO:");
		System.out.println(e1);
		System.out.println();
		Electrodomestico e2 = new Electrodomestico(5000, 230);
		System.out.println("Un constructor con el precio y peso. El resto por defecto:");
		System.out.println(e2);
		System.out.println();
		Electrodomestico e3 = new Electrodomestico(3000, "Amarillo", 'B', 200);
		System.out.println("Un constructor con todos los atributos:");
		System.out.println(e3);
		e3.comprovarColor(e3);


	}

}
