package dto;

public class Electrodomestico {
	
	final int precioB= 100;
	final String c = "blanco";
	final char consumo = 'F';
	final double p = 5;
	
	private int precioBase;
	private String color;
	private char consumoElectrico;
	private double peso;
	
	//CONSTRUCTOR POR DEFECTO
	public Electrodomestico() {
		super();
		this.precioBase = this.precioB;
		this.color=this.c;
		this.consumoElectrico = this.consumo;
		this.peso = this.p;
				
	}
	
	
	//Un constructor con el precio y peso. El resto por defecto
	public Electrodomestico(int precioBase, double peso) {
		super();
		this.precioBase = precioBase;
		this.color=this.c;
		this.consumoElectrico = this.consumo;
		this.peso = peso;
	}



	//Un constructor con todos los atributos
	public Electrodomestico(int precioBase, String color, char consumoElectrico, double peso) {
		super();
		this.precioBase = precioBase;
		this.color = color;
		this.consumoElectrico = consumoElectrico;
		this.peso = peso;
	}



	@Override
	public String toString() {
		return "Electrodomestico [precioB=" + precioB + ", c=" + c + ", consumo=" + consumo + ", p=" + p
				+ ", precioBase=" + precioBase + ", color=" + color + ", consumoElectrico=" + consumoElectrico
				+ ", peso=" + peso + "]";
	}
	
	
	//GET
	public String getColor() {
		return color;
	}



		//METODO PARA COMPROBAR SI EL COLOR ESTA DISPONIBLE
	public static void comprovarColor(Electrodomestico color) {
		
		switch (color.getColor()) {
		case "blanco":
			System.out.println("COLOR DISPONIBLE");
			break;
		case "BLANCO":
			System.out.println("COLOR DISPONIBLE");

			break;
		case "negro":
			System.out.println("COLOR DISPONIBLE");

			break;
		case "NEGRO":
			System.out.println("COLOR DISPONIBLE");

			break;
		case "rojo":
			System.out.println("COLOR DISPONIBLE");

			break;
		case "ROJO":
			System.out.println("COLOR DISPONIBLE");

			break;
		case "azul":
			System.out.println("COLOR DISPONIBLE");

			break;
		case "AZUL":
			System.out.println("COLOR DISPONIBLE");

			break;
		case "gris":
			System.out.println("COLOR DISPONIBLE");

			break;
		case "GRIS":
			System.out.println("COLOR DISPONIBLE");

			break;

		default:
			System.out.println("COLOR NO DISPONIBLE");

			break;
		}
	}
	
	
	
	
}
